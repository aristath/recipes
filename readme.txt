=== Recipes ===
Contributors: aristath
Tags: recipes, cooking, custom post type, custom taxonomy
Requires at least: 4.4
Tested up to: 4.6
Stable tag: 1.0.0
License: GPLv3 or later
License URI: http://www.gnu.org/licenses/gpl-3.0.html

Recipes for WordPress done right.

== Description ==

The Recipes plugin allows you to write recipes, add ingredients and steps with simplicity in mind.

Create your recipes, add ingredients, steps, a short description as well as a full description and recipe categories!
Includes support for featured images and an easy to use template.

== Installation ==

Simply install as a normal WordPress plugin and activate.

== Changelog ==

= 1.0.0 =

August 1, 2016

* Initial version
